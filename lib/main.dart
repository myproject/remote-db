import 'package:flutter/material.dart';
import 'package:remote_db/view/LoadPage.dart';

void main() {
  Color myBlueColor = Color.fromRGBO(10, 50, 190, 1);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Agenda orion',
        home: LoadPage(),
      );
    }
  }
}
