import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyBlue.dart';

class MyAppBar {
  AppBar get() {
    return AppBar(
      backgroundColor: MyBlue().get(),
      title: Text("Remote DB"),
    );
  }
}
