import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyGrey.dart';
import 'package:remote_db/myColors/MyLightBlue.dart';

class MyDrawer {
  Drawer get() {
    return Drawer(
      backgroundColor: MyGrey().get(),
      child: ListView(
        children: [
          DrawerHeader(
            // TODO: add icon app
            child: Text(
              "Remote DB by NuRoZ",
              textAlign: TextAlign.justify,
              style: TextStyle(color: Colors.white),
            ),
            decoration: BoxDecoration(color: MyLightBlue().get()),
          ),
        ],
      ),
    );
  }
}
