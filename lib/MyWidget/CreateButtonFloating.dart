import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyBlue.dart';
import 'package:remote_db/view/AddDB.dart';

class CreateButtonFloating {
  ElevatedButton get(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => AddDB()));
      },
      child: Icon(Icons.add),
      style: ButtonStyle(
          shape: MaterialStateProperty.all<OutlinedBorder?>(CircleBorder()),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry?>(
              EdgeInsets.all(15.0)),
          backgroundColor: MyBlue().getProperty()),
    );
  }
}
