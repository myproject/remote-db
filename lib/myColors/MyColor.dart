import 'package:flutter/material.dart';

abstract class MyColors {
  Color get();
  MaterialStateProperty<Color?> getProperty() {
    return MaterialStateProperty.all<Color?>(get());
  }
}
