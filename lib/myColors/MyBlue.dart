import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyColor.dart';

class MyBlue extends MyColors {
  @override
  Color get() {
    return Color.fromRGBO(10, 50, 190, 1);
  }
}
