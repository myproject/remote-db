import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyColor.dart';

class MyGrey extends MyColors {
  @override
  Color get() {
    return Color.fromRGBO(48, 48, 48, 1);
  }
}
