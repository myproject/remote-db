import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyColor.dart';

class MyLightGrey extends MyColors {
  @override
  Color get() {
    return Color.fromRGBO(66, 66, 66, 1);
  }
}
