import 'package:flutter/material.dart';
import 'package:remote_db/myColors/MyColor.dart';

class MyLightBlue extends MyColors {
  @override
  Color get() {
    return Color.fromRGBO(0, 83, 255, 1);
  }
}
