import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';

class MyFile {
  static Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<File> get _localFileSaveDatabase async {
    final path = await _localPath;

    bool fileExists = await File("$path/saveDatabase.txt").exists();
    if (!(fileExists)) {
      await File("$path/saveDatabase.txt").create();
    }

    return File('$path/saveDatabase.txt');
  }

  static Future<File> addSaveDatabaseFile(String databaseName) async {
    final file = await _localFileSaveDatabase;

    String content = await getSaveDatabaseFile();
    return file.writeAsString(content + databaseName + ",");
  }

  static Future<File> removeSaveDatabaseFile(String removeSaveDatabase) async {
    final file = await _localFileSaveDatabase;
    List<String> databases = await getSaveDatabaseFile().then((value) {
      return value.split(",");
    });

    String result = "";

    for (String database in databases) {
      if (database != removeSaveDatabase && database != "") {
        result += database + ",";
      }
    }

    return file.writeAsString(result);
  }

  static Future<String> getSaveDatabaseFile() async {
    String resultat = "";
    try {
      final File = await _localFileSaveDatabase;

      resultat = await File.readAsString();
    } catch (e) {
      print(e.toString());
    }

    return resultat;
  }
}
