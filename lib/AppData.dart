import 'package:remote_db/model/Database.dart';

class AppData {
  static List<Database> lesDatabases = [];
  static late Database manageDatabase;

  static void setLesDatabases(List<Database> databases) {
    lesDatabases = databases;
  }

  static List<Database> getLesDatabases() {
    return lesDatabases;
  }

  static void removeDatabase(Database database) {
    lesDatabases.remove(database);
  }

  static void setManageDatabase(Database database) {
    manageDatabase = database;
  }

  static Database getManageDatabase() {
    return manageDatabase;
  }
}
