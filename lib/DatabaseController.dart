import 'package:remote_db/AppData.dart';
import 'package:remote_db/MyFile.dart';
import 'package:remote_db/MySecureFile.dart';
import 'package:remote_db/model/Database.dart';

class DatabaseController {
  static DatabaseController instance = DatabaseController();
  static DatabaseController getInstance() {
    return DatabaseController();
  }

  Future<void> save(Database database) async {
    await MySecureFile.setDatabaseSecureFile(
        database.databaseName, database.toJson());
    await MyFile.addSaveDatabaseFile(database.databaseName);
    List<Database> newAppData = AppData.getLesDatabases();
    newAppData.add(database);
    AppData.setLesDatabases(newAppData);
  }
}
