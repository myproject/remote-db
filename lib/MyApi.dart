import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:remote_db/model/ColumnMaria.dart';
import 'package:remote_db/model/ColumnPostgre.dart';
import 'package:remote_db/model/Database.dart';
import 'package:remote_db/model/TableMaria.dart';
import 'package:remote_db/model/TablePostgre.dart';

class MyApi {
  static String url = "https://remotedb.k-gouzien.fr/index.php";

  static Future<bool> testConnexion(String host, String databaseName,
      String username, String password, String databaseType) async {
    var uri = Uri.parse("$url");
    var request = http.MultipartRequest('POST', uri)
      ..fields["host"] = host
      ..fields["databaseName"] = databaseName
      ..fields["username"] = username
      ..fields["password"] = password
      ..fields["databaseType"] = databaseType;

    var response = await request.send();
    return await response.stream.bytesToString() == "true" ? true : false;
  }

  static Future<List<TableMaria>> getTableMaria(Database database) async {
    List<TableMaria> lesTables = [];

    var uri = Uri.parse("$url");
    var request = http.MultipartRequest('POST', uri)
      ..fields["host"] = database.host
      ..fields["databaseName"] = database.databaseName
      ..fields["username"] = database.username
      ..fields["password"] = database.password
      ..fields["databaseType"] = database.databaseType
      ..fields["action"] = "getTables";

    var response = await request.send();
    String tableJson = await response.stream.bytesToString();

    var tableDecode = jsonDecode(tableJson);
    // print(tableDecode["tables"][0]["competence"][0]["Name"]);
    for (var uneTable in tableDecode["Tables"]) {
      //print(uneTable["Name"]);
      List<ColumnMaria> lesColumns = [];
      for (var uneColumn in uneTable["Columns"]) {
        lesColumns.add(ColumnMaria(
            uneColumn["Name"],
            uneColumn["Type"],
            uneColumn["Nullable"],
            uneColumn["Key"],
            uneColumn["Default"],
            uneColumn["Extra"]));
      }
      lesTables.add(TableMaria(uneTable["Name"], lesColumns));
    }

    return lesTables;
  }

  static Future<List<TablePostgre>> getTablePostgre(Database database) async {
    List<TablePostgre> lesTables = [];

    var uri = Uri.parse("$url");
    var request = http.MultipartRequest('POST', uri)
      ..fields["host"] = database.host
      ..fields["databaseName"] = database.databaseName
      ..fields["username"] = database.username
      ..fields["password"] = database.password
      ..fields["databaseType"] = database.databaseType
      ..fields["action"] = "getTables";

    var response = await request.send();
    String tableJson = await response.stream.bytesToString();

    var tableDecode = jsonDecode(tableJson);
    // print(tableDecode["tables"][0]["competence"][0]["Name"]);
    for (var uneTable in tableDecode["Tables"]) {
      //print(uneTable["Name"]);
      List<ColumnPostgre> lesColumns = [];
      for (var uneColumn in uneTable["Columns"]) {
        lesColumns.add(ColumnPostgre(
          uneColumn["Name"],
          uneColumn["Type"],
          uneColumn["Nullable"],
        ));
      }
      lesTables.add(TablePostgre(uneTable["Name"], lesColumns));
    }

    return lesTables;
  }
}
