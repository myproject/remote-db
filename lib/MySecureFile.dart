import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:remote_db/model/Database.dart';

class MySecureFile {
  static const FlutterSecureStorage _storage = FlutterSecureStorage(
      aOptions: AndroidOptions(encryptedSharedPreferences: true));

  static Future setDatabaseSecureFile(
      String databaseName, String databaseJson) async {
    await _storage.write(key: databaseName, value: databaseJson);
  }

  static Future<Database?> getDatabaseSecureFile(String databaseName) async {
    return Database.toObject(await _storage.read(key: databaseName));
  }

  static Future<void> removeDatabaseSecureFile(String databaseName) async {
    await _storage.delete(key: databaseName);
  }
}
