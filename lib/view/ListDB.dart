import 'package:flutter/material.dart';
import 'package:remote_db/AppData.dart';
import 'package:remote_db/MyFile.dart';
import 'package:remote_db/MySecureFile.dart';
import 'package:remote_db/myColors/MyGrey.dart';
import 'package:remote_db/MyWidget/MyDrawer.dart';
import 'package:remote_db/MyWidget/CreateButtonFloating.dart';
import 'package:remote_db/MyWidget/MyAppBar.dart';
import 'package:remote_db/model/Database.dart';
import 'package:remote_db/myColors/MyLightGrey.dart';
import 'package:remote_db/view/DetailDB.dart';

class ListDB extends StatefulWidget {
  @override
  _ListDB createState() => _ListDB();
}

class _ListDB extends State<ListDB> {
  List<Database> lesDatabases = AppData.getLesDatabases();
  @override
  Widget build(BuildContext context) {
    {
      return Scaffold(
          backgroundColor: MyGrey().get(),
          drawer: MyDrawer().get(),
          appBar: MyAppBar().get(),
          floatingActionButton: CreateButtonFloating().get(context),
          body: SingleChildScrollView(
            child: Column(
              children: [
                for (Database database in lesDatabases)
                  if (database.databaseName != "")
                    Padding(
                        padding: EdgeInsets.only(
                            top: 10,
                            bottom: 10,
                            left: MediaQuery.of(context).size.width * 0.1),
                        child: ElevatedButton(
                            style: ButtonStyle(
                                minimumSize: MaterialStateProperty.all<Size?>(
                                    Size(
                                        MediaQuery.of(context).size.width * 0.8,
                                        0)),
                                backgroundColor:
                                    MaterialStateProperty.all<Color?>(
                                        MyLightGrey().get()),
                                padding: MaterialStateProperty.all<
                                        EdgeInsetsGeometry?>(
                                    EdgeInsets.only(top: 15, bottom: 15))),
                            onPressed: () {
                              AppData.setManageDatabase(database);

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailDB()));
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  if (database.databaseType == "postgre")
                                    Image.asset(
                                      'images/postgreLogo.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height:
                                          MediaQuery.of(context).size.width *
                                              0.3,
                                    ),
                                  if (database.databaseType == "mariasql")
                                    Image.asset(
                                      'images/mysqlLogo.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height:
                                          MediaQuery.of(context).size.width *
                                              0.3,
                                    ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                            0.8 -
                                        MediaQuery.of(context).size.width *
                                            0.3 -
                                        50,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Icon(Icons.filter_drama_outlined),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(right: 10)),
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.28,
                                              child: Text(
                                                database.databaseName,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                        ]),
                                        Row(children: [
                                          Icon(Icons.wifi),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(right: 10)),
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.28,
                                              child: Text(
                                                database.host,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                        ]),
                                        Row(children: [
                                          Icon(Icons.account_circle),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(right: 10)),
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.28,
                                              child: Text(
                                                database.username,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                        ])
                                      ],
                                    ),
                                  ),
                                  Column(children: [
                                    PopupMenuButton(
                                        color: MyLightGrey().get(),
                                        itemBuilder: (context) => [
                                              PopupMenuItem(
                                                textStyle: TextStyle(
                                                    color: Colors.white),
                                                child: Text("Supprimer"),
                                                onTap: () {
                                                  MyFile.removeSaveDatabaseFile(
                                                          database.databaseName)
                                                      .then((value) {
                                                    MySecureFile
                                                            .removeDatabaseSecureFile(
                                                                database
                                                                    .databaseName)
                                                        .then((value) {
                                                      AppData.removeDatabase(
                                                          database);
                                                      setState(() {
                                                        lesDatabases = AppData
                                                            .getLesDatabases();
                                                      });
                                                    });
                                                  });
                                                },
                                              )
                                            ],
                                        icon: Icon(
                                          Icons.settings,
                                        )),
                                    Container(
                                      height: 59,
                                    ),
                                    Container(
                                        width: 10,
                                        height: 10,
                                        decoration: BoxDecoration(
                                          color: database.isActive
                                              ? Colors.green
                                              : Colors.red,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                        ))
                                  ])
                                ],
                              ),
                            )))
              ],
            ),
          ));
    }
  }
}
