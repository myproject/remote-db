import 'package:flutter/material.dart';
import 'package:remote_db/AppData.dart';
import 'package:remote_db/model/Database.dart';
import 'package:remote_db/MyApi.dart';
import 'package:remote_db/model/MyTable.dart';

class DetailDB extends StatefulWidget {
  _DetailDB createState() => _DetailDB();
}

class _DetailDB extends State<DetailDB> {
  Database database = AppData.getManageDatabase();
  List<MyTable> lesTables = [];

  @override
  void initState() {
    super.initState();
    if (database.databaseType == "postgre") {
      MyApi.getTablePostgre(database).then((value) {
        setState(() {
          lesTables = value;
        });
      });
    } else if (database.databaseType == "mariasql") {
      MyApi.getTableMaria(database).then((value) {
        setState(() {
          lesTables = value;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          for (MyTable table in lesTables) Text(table.nom),
        ],
      ),
    );
  }
}
