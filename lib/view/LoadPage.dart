import 'package:flutter/material.dart';
import 'package:remote_db/AppData.dart';
import 'package:remote_db/model/Database.dart';
import 'package:remote_db/MyApi.dart';
import 'package:remote_db/MyFile.dart';
import 'package:remote_db/MySecureFile.dart';
import 'package:remote_db/myColors/MyGrey.dart';
import 'package:remote_db/view/ListDB.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class LoadPage extends StatefulWidget {
  @override
  _LoadPage createState() => _LoadPage();
}

class _LoadPage extends State<LoadPage> {
  double advencementProgressBar = 0;
  bool hasInternet = true;

  Future<bool> TestConnexion() async {
    return await InternetConnectionChecker().hasConnection;
  }

  void loadData(BuildContext context) async {
    if (await TestConnexion()) {
      setState(() {
        hasInternet = true;
      });

      String nameDatabases = await MyFile.getSaveDatabaseFile();
      //10% progress
      setState(() {
        advencementProgressBar =
            (MediaQuery.of(context).size.width * 0.8) * 0.1;
      });

      List<String> lesDatabasesName = nameDatabases.split(",");
      int nbDatabase = lesDatabasesName.length;

      //90 / 2
      double nbPercentParDatabase =
          ((MediaQuery.of(context).size.width * 0.8) * 0.45) / nbDatabase;

      List<Database> lesDatabasesWIP = [];
      for (String databaseName in lesDatabasesName) {
        //45 / nbdatabase (nbPercentDatabase)
        setState(() {
          advencementProgressBar += nbPercentParDatabase;
        });

        var uneDatabase =
            await MySecureFile.getDatabaseSecureFile(databaseName);
        if (uneDatabase != null) {
          lesDatabasesWIP.add(uneDatabase);
        }
      }
      AppData.setLesDatabases(lesDatabasesWIP);

      for (Database uneDatabase in lesDatabasesWIP) {
        //45 / nbdatabase (nbPercentDatabase)
        setState(() {
          advencementProgressBar += nbPercentParDatabase;
        });

        uneDatabase.isActive = await MyApi.testConnexion(
            uneDatabase.host,
            uneDatabase.databaseName,
            uneDatabase.username,
            uneDatabase.password,
            uneDatabase.databaseType);
      }

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ListDB()),
      );
    } else {
      setState(() {
        hasInternet = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    loadData(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyGrey().get(),
        body: Column(children: [
          if (hasInternet)
            Stack(
              alignment: Alignment.centerLeft,
              children: [
                Center(
                    child: Image.asset(
                  "images/loadGif.jpg",
                  width: 60,
                  height: 60,
                )),
                Container(
                  height: 20,
                  width: MediaQuery.of(context).size.width * 0.8,
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1,
                      top: MediaQuery.of(context).size.height * 0.5),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
                Container(
                  height: 20,
                  width: advencementProgressBar,
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1,
                      top: MediaQuery.of(context).size.height * 0.5),
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
              ],
            ),
          if (!hasInternet)
            Align(
                heightFactor: 9,
                alignment: Alignment.center,
                child: Column(children: [
                  Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text(
                        "No internet connection",
                        style: TextStyle(color: Colors.red),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        loadData(context);
                      },
                      child: Text("Retry"))
                ]))
        ]));
  }
}
