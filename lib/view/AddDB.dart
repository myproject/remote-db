import 'package:flutter/material.dart';
import 'package:remote_db/AppData.dart';
import 'package:remote_db/MyApi.dart';
import 'package:remote_db/model/Database.dart';
import 'package:remote_db/myColors/MyGrey.dart';
import 'package:remote_db/myColors/MyLightGrey.dart';
import 'package:remote_db/MyWidget/MyAppBar.dart';
import 'package:remote_db/DatabaseController.dart';
import 'package:remote_db/view/ListDB.dart';
import 'package:remote_db/view/LoadPage.dart';

class AddDB extends StatefulWidget {
  _AddDB createState() => _AddDB();
}

class _AddDB extends State<AddDB> {
  String host = "";
  String database = "";
  String username = "";
  String password = "";
  String dataBaseValue = "mariasql";
  bool isTested = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar().get(),
      backgroundColor: MyGrey().get(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.94,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(color: MyLightGrey().get()),
                child: TextField(
                  onChanged: (value) {
                    host = value.trim();
                    isTested = false;
                  },
                  decoration: InputDecoration(
                    hintText: "Hostname / IP address",
                    hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                    icon: Icon(Icons.wifi),
                  ),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.94,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(color: MyLightGrey().get()),
                child: TextField(
                  onChanged: (value) {
                    database = value.trim();
                    isTested = false;
                  },
                  decoration: InputDecoration(
                    hintText: "Database name",
                    hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                    icon: Icon(Icons.filter_drama_outlined),
                  ),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.94,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(color: MyLightGrey().get()),
                child: TextField(
                  onChanged: (value) {
                    username = value.trim();
                    isTested = false;
                  },
                  decoration: InputDecoration(
                    hintText: "Username",
                    hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                    icon: Icon(Icons.account_circle),
                  ),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.94,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(color: MyLightGrey().get()),
                child: TextField(
                  obscureText: true,
                  onChanged: (value) {
                    password = value.trim();
                    isTested = false;
                  },
                  decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle:
                          TextStyle(color: Colors.white.withOpacity(0.5)),
                      icon: Icon(Icons.vpn_key_rounded)),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.94,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(color: MyLightGrey().get()),
                child: Row(children: [
                  Icon(
                    Icons.outlet,
                    color: MyGrey().get(),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.87,
                    padding: EdgeInsets.only(left: 15),
                    child: DropdownButton<String>(
                      dropdownColor: MyLightGrey().get(),
                      value: dataBaseValue,
                      hint: Text("Database type"),
                      style: TextStyle(color: Colors.white),
                      items: [
                        DropdownMenuItem(
                            child: Text("Mariadb/MySql"), value: "mariasql"),
                        DropdownMenuItem(
                            child: Text("PostgreSql"), value: "postgre")
                      ].toList(),
                      onChanged: (value) {
                        setState(() {
                          dataBaseValue = value as String;
                        });
                        isTested = false;
                      },
                    ),
                  ),
                ]),
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.04,
                        0,
                        MediaQuery.of(context).size.width * 0.02,
                        0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (host != "" &&
                            database != "" &&
                            username != "" &&
                            password != "") {
                          MyApi.testConnexion(host, database, username,
                                  password, dataBaseValue)
                              .then((value) {
                            if (value) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text("Connected !"),
                                backgroundColor: Colors.green,
                              ));
                              isTested = true;
                            } else {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text("not connected to database."),
                                backgroundColor: Colors.red,
                              ));
                            }
                          });
                        } else {
                          String notFill = "";
                          if (host == "") {
                            notFill += "host, ";
                          }
                          if (database == "") {
                            notFill += "database, ";
                          }
                          if (username == "") {
                            notFill += "username, ";
                          }
                          if (password == "") {
                            notFill += "password, ";
                          }
                          notFill.replaceRange(
                              notFill.length - 2, notFill.length - 1, "");
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(notFill + "not fill !"),
                            backgroundColor: Colors.red,
                          ));
                        }
                      },
                      child: Text("Test connexion"),
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all<Size?>(
                            Size(MediaQuery.of(context).size.width * 0.44, 35)),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.02,
                        0,
                        MediaQuery.of(context).size.width * 0.04,
                        0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (isTested) {
                          bool databaseAlreadyExist = false;
                          for (Database uneDatabase
                              in AppData.getLesDatabases()) {
                            if (uneDatabase.databaseName == database) {
                              databaseAlreadyExist = true;
                            }
                          }

                          if (databaseAlreadyExist) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("You already have this database"),
                              backgroundColor: Colors.red,
                            ));
                          } else {
                            DatabaseController.getInstance()
                                .save(Database(host, database, username,
                                    password, dataBaseValue, true))
                                .then((value) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListDB()));
                            });
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(
                                "you need test the connection before save."),
                            backgroundColor: Colors.red,
                          ));
                        }
                      },
                      child: Text("Save"),
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all<Size?>(
                            Size(MediaQuery.of(context).size.width * 0.44, 35)),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
