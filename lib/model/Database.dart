import 'dart:convert';

class Database {
  static Database toObject(String? json) {
    Database res = Database("", "", "", "", "", false);

    if (json != null) {
      Map<String, dynamic> databaseDecode = jsonDecode(json);

      res = Database(
          databaseDecode["host"],
          databaseDecode["databaseName"],
          databaseDecode["username"],
          databaseDecode["password"],
          databaseDecode["databaseType"],
          false);
    }

    return res;
  }

  String host = "";
  String databaseName = "";
  String username = "";
  String password = "";
  String databaseType = "";
  bool isActive = false;

  Database(this.host, this.databaseName, this.username, this.password,
      this.databaseType, this.isActive);

  toJson() {
    return '{"host": "' +
        host +
        '", "username": "' +
        username +
        '", "databaseName": "' +
        databaseName +
        '", "password": "' +
        password +
        '", "databaseType": "' +
        databaseType +
        '"}';
  }
}
