class ColumnPostgre {
  String nom = "";
  String type = "";
  String nullable = "";

  ColumnPostgre(this.nom, this.type, this.nullable);

  String getNom() {
    return nom;
  }

  String getType() {
    return type;
  }

  String getNullable() {
    return nullable;
  }
}
