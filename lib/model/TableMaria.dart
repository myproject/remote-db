import 'package:remote_db/model/ColumnMaria.dart';
import 'package:remote_db/model/MyTable.dart';

class TableMaria extends MyTable {
  List<ColumnMaria> lesColumns = [];

  TableMaria(String name, List<ColumnMaria> collumns) : super(name) {
    lesColumns = collumns;
  }

  void addColumns(ColumnMaria column) {
    lesColumns.add(column);
  }
}
