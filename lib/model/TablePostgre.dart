import 'package:remote_db/model/ColumnPostgre.dart';
import 'package:remote_db/model/MyTable.dart';

class TablePostgre extends MyTable {
  List<ColumnPostgre> lesColumns = [];

  TablePostgre(nom, this.lesColumns) : super(nom);

  void addColumns(ColumnPostgre column) {
    lesColumns.add(column);
  }
}
