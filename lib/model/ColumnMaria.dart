class ColumnMaria {
  String nom = "";
  String type = "";
  String nullable = "";
  String key = "";
  String defaultValue = "";
  String extra = "";

  ColumnMaria(this.nom, this.type, this.nullable, this.key, this.defaultValue,
      this.extra);

  String getNom() {
    return nom;
  }

  String getType() {
    return type;
  }

  String getNullable() {
    return nullable;
  }

  String getKey() {
    return key;
  }

  String getDefaultValue() {
    return defaultValue;
  }

  String getExtra() {
    return extra;
  }
}
